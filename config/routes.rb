Rails.application.routes.draw do
  root 						         'static_pages#home'

  get 'help' 		        => 'static_pages#help'

  get 'about'			      => 'static_pages#about'
 
  get 'projects' 	      => 'static_pages#projects'

	get 'weather'         => 'weather#index'

	post 'weather'        => 'weather#index'

	get 'scraper'         => 'page_scraper#index'

	get 'scraper/show'    => 'page_scraper#show'

	post 'scraper/show'   => 'page_scraper#show'

	get 'scraper/reddit'  => 'page_scraper#reddit'

	post 'scraper/reddit' => 'page_scraper#reddit'
	
end
