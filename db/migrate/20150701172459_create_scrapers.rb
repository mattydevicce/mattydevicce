class CreateScrapers < ActiveRecord::Migration
  def change
    create_table :scrapers do |t|
      t.string :city
      t.string :state

      t.timestamps null: false
    end
  end
end
