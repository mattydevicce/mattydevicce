class Scraper < ActiveRecord::Base
	attr_accessor :theTemperature, :tempUpdated, :icon, :weather_description, :dateFirst, :tempHighFirst, :tempLowFirst,
								:weatherTest, :error, :error_description, :city, :state

	def check_params_current(city, state)
		check_current_weather_hash = fetch_current_temperature(city, state)
		current_weather_response = check_current_weather_hash.parsed_response['response']
		if current_weather_response['error']
			self.error = current_weather_response['type']
			self.error_description = current_weather_response['description']
			good_params = [city = "Port Washington", state = "NY"]
			return good_params
		else
			good_params = [city, state]
			return good_params
		end
	end
	
	def check_params_10Day(city, state)
		check_forecast_hash = fetch_10DayForecast(city, state)
		current_weather_response = check_forecast_hash.parsed_response['response']
		if current_weather_response['error']
			self.error = current_weather_response['type']
			self.error_description = current_weather_response['description']
			good_params = [city = "Port Washington", state = "NY"]
			return good_params
		else
			good_params = [city, state]
			return good_params
		end
	end

	def fetch_current_temperature(city, state)
		city.sub! ' ', '_'
		HTTParty.get("http://api.wunderground.com/api/db1ec1e1328bd050/conditions/q/" + state.to_s + "/" + city.to_s + ".xml")
	end

	def fetch_10DayForecast(city, state)
		city.sub! ' ', '_'
		HTTParty.get("http://api.wunderground.com/api/db1ec1e1328bd050/forecast10day/q/" + state.to_s + "/" + city.to_s + ".xml")
	end

	def initialize(dparams)
		currentCity  = check_params_current(dparams[:city], dparams[:state])[0]
		currentState = check_params_current(dparams[:city], dparams[:state])[1]
		current_weather_hash = fetch_current_temperature(currentCity, currentState)
		current_weather(current_weather_hash, currentCity, currentState)
		forecast_hash = fetch_10DayForecast(check_params_10Day(dparams[:city], dparams[:state])[0],check_params_10Day(dparams[:city], dparams[:state])[1])
		forecast(forecast_hash)
	end

	def current_weather(cw, city, state)
		current 								 = cw.parsed_response['response']['current_observation']
		self.city 							 = city
		self.state				 			 = state
		self.theTemperature 		 = current['temp_f']
		self.tempUpdated 				 = current['observation_time']
		self.icon 							 = current['icon_url']
		self.weather_description = current['icon']
	end

	def forecast(fc)
		forecast_first 		 	= fc.parsed_response['response']['forecast']['simpleforecast']['forecastdays']['forecastday'][0]
		self.dateFirst 		  = forecast_first['date']['monthname'] + forecast_first['date']['day']
		self.tempHighFirst  = forecast_first['high']['fahrenheit']
		self.tempLowFirst  	= forecast_first['low']['fahrenheit']
		self.weatherTest    = fc.parsed_response['response']['forecast']['simpleforecast']['forecastdays']['forecastday']
	end

	private


end
