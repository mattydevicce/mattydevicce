class GetPage < ActiveRecord::Base

	attr_accessor :list

	require 'nokogiri'
	require 'open-uri'
	
	def get_page(link)
		doc = Nokogiri::HTML(open('http://reddpics.com/r/' + link.to_s)) 
		return doc
	end

	def initialize(link)
		page = get_page(link)
		the_list(page)
	end

	def the_list(page)
		
		link_list = {}
		page.css("#images li").each do |node|
			
			get_link = node.at_css('a')['href']
			good_link = get_link unless get_link.include? 'google' or get_link.include? 'reddit' or get_link.exclude? 'jpg'

			edit_title = node.at_css('h2 a').text
			no_want1 = edit_title.index('[')
			no_want2 = edit_title.index('(')
			if edit_title.include? '['
				title = edit_title[0...no_want1]
			elsif edit_title.include? '('
				title = edit_title[0...no_want2]
			else
				title = edit_title
			end
			link_list[title] = good_link unless link_list.include? good_link
		end


		self.list = link_list
	end

end
