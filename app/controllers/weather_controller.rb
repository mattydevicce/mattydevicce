class WeatherController < ApplicationController

	def index
		location = get_location
		@scraper = Scraper.new(location)
	end

	private

		def get_location

			uloca = { :city => params[:city], :state => params[:state] }
			loca  = { :state => "NY", :city => "Port_Washington" }

			if uloca[:city].blank?
				return loca
			elsif uloca[:city].gsub(" ", "_").match(/\A[a-z_]+\z/i)
				@notice = "Weather location successful"
				return uloca
			else
				@notice = "Something isn't correct"
			end
			return loca
		end

end
